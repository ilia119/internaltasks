package by.igoroshko;

public class ElementsPermutation {

    private int[][] readyArray = null;
    private static int addedElem;

    public void permutate(int[] arr) {
        if (arr.length > 8) {
            throw new ArrayIndexOutOfBoundsException("Too much elements");
        }
        if (arr.length == 0 || arr.length == 1) {
            return;
        }
        int count = factorial(arr.length);
        int max = arr.length - 1;
        readyArray = new int[count][arr.length];
        addedElem = 0;
        int shift = max;
        while (count > 0) {
            int t = arr[shift];
            arr[shift] = arr[shift - 1];
            arr[shift - 1] = t;
            print(arr);
            addedElem++;
            count--;
            if (shift < 2) {
                shift = max;
            } else {
                shift--;
            }
        }
    }

    public void print(int[] arr) {
        readyArray[addedElem] = arr;
    }

    public int factorial(int n) {
        return (n > 0) ? n * factorial(n - 1) : 1;
    }

    public int[][] getReadyArray() {
        return readyArray;
    }

}

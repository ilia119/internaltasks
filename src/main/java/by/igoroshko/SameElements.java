package by.igoroshko;

public class SameElements {

    public boolean isSameElements(int[] arr) {
        if (arr.length > 100) {
            return false;
        }

        for (int i = 1; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    return true;
                }
            }
        }
        return false;
    }
}

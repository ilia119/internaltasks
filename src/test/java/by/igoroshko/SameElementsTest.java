package by.igoroshko;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SameElementsTest {

    @Test
    public void falseArray() {
        int[] arr = new int[100];
        int j = 1;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = j;
            j++;
        }
        assertFalse(new SameElements().isSameElements(arr));
    }

    @Test
    public void false101Elements() {
        int[] arr = new int[]{1, 2, 3, 2 ,5, 6, 4};
        assertTrue(new SameElements().isSameElements(arr));
    }

    @Test
    public void falseLargeArray() {
        int[] arr = new int [1000];
        assertFalse(new SameElements().isSameElements(arr));
    }
}

package by.igoroshko;

import org.junit.Test;

import static org.junit.Assert.*;

public class ElementsPermutationTest {

    @Test
    public void emptyArray(){
        new ElementsPermutation().permutate(new int[0]);
    }

    @Test
    public void oneElementArray(){
        new ElementsPermutation().permutate(new int[1]);
    }

    @Test (expected = ArrayIndexOutOfBoundsException.class)
    public void moreThen9ArrayElements() {
        new ElementsPermutation().permutate(new int[9]);
    }

    @Test
    public void trueArray () {
        ElementsPermutation elementsPermutation = new ElementsPermutation();
        elementsPermutation.permutate(new int[] {1, 2, 3, 4, 5});
        assertEquals(120, elementsPermutation.getReadyArray().length);
    }

    @Test
    public void trueArray2 () {
        ElementsPermutation elementsPermutation = new ElementsPermutation();
        elementsPermutation.permutate(new int[] {1, 2, 3});
        assertEquals(6, elementsPermutation.getReadyArray().length);
    }

}